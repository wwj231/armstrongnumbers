
public class ArmstrongNumbers {

    public static boolean isArmstrongNumber(int number) {
        int n = countDigits(number);
        int temp=number;
        double sum=0;

        while (temp!=0) {
            int r = temp%10;
            sum = sum + Math.pow(r,n);
            temp = temp/10;
        }
        return sum == number;
    }


    private static int countDigits(int number){
        String string = String.valueOf(number);
        return string.length();
    }

}
